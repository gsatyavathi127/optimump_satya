import React from 'react';
import Banner from  './Banner'
import Management from "./Management";
import Features from "./Features";

function Home() {
  return (
    <div className='home_wrapper'>
        <Banner />
        <Management />
        <Features />
    </div>
  )
}

export default Home