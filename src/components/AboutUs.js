import abImg1 from "../assets/ab1.jpg";
import abImg2 from "../assets/ab2.jpg";
import abImg3 from "../assets/ab3.jpg";
import icon1 from "../assets/icon1.jpg";
import icon2 from "../assets/icon2.jpg";
import React from "react";
// import Header from "/Header";
// import Footer from "/Footer";
function About() {
  return (
    <div className="about_wrapper">
      <div className="px-5">
        <div className="about_content">
          <h1>About OptimumP</h1>
        </div>
        <div className="row">
          <div className="col-6">
          <img src={abImg1}/>
    
          </div>
          <div className="col-6">
            <h2>We want the web to win</h2>
            <p>
              The OptimumP BMS is a full featured lithium ion battery management
              system that is specifically designed to meet the tough
              requirements of protecting and managing battery packs for electric
              vehicles (EV), plug-in hybrid (PHEV) and hybrid vehicles (HEV)
              with automotive grade quality.
            </p>
          </div>
        </div>
        <div className="row">
          <div className="col-6">
            <h2>A company is as good as its team</h2>
            <p>
              We are an energy management company that specializes in charging
              and battery management systems (BMS).
            </p>
            <p>
              We are passionate about mobility and passionate about a clean
              environment. We’ve spent years driving innovation in mobility, yet
              we know there is still so much more to be done. We know we can all
              do better. We can create an amazing mobility experience while
              still protecting the health of our future generations.
            </p>
          </div>
          <div className="col-6">
            <img src={abImg2}/>
          </div>
        </div>
        <div className="row">
          <div className="col-6">
            <h2><img src={icon1} style={{width:40}}/> Culture</h2>
            <p>
              If a company is as good as the team of people who work there, then
              a team is as good as its culture. We aim to be as intentional as
              we can in how we scale both our organization and culture.
            </p>
          </div>
          <div className="col-6">
            <h2><img src={icon2} style={{width:40}}/> Distribution</h2>
            <p>
              If a company is as good as the team of people who work there, then
              a team is as good as its culture. We aim to be as intentional as
              we can in how we scale both our organization and culture.
            </p>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <h2>Why OptimumP</h2>
            <ul>
              <li>Builds trust and accountability.</li>
              <li>Shows unity through a shared vision.</li>
              <li>
                Helps us communicate externally who we are as a company and as a
                team.
              </li>
              <li>
                Gives us a framework to ground our decision making when
                conversations get tough.
              </li>
              <li>
                Flattens the organization. Guided by these values, critical and
                complimentary feedback alike can come from anywhere in the
                company.
              </li>
              <li>
                Sets expectations for what to expect from peers, managers, and
                leadership.
              </li>
            </ul>
          </div>
        </div>
        <div className="row">
          <div className="col-6">
            <img src={abImg3}/>
          </div>
          <div className="col-6">
            <h2>Careers</h2>
            <p>
              We’re a tightly knit team of driven, mission-focused people. We’re
              growing rapidly while still nurturing our culture by carefully
              adding passionate and empathetic people.
            </p>
            <p>
              We have lots of open positions. That said, we hire people, not
              roles. Together we will carve out the right fit that gives you
              maximum joy and sets you up for success, while making sure it’s
              what we need to keep taking strides in creating a better web. For
              all of us.
            </p>
            <p>Can you find yourself in the above? Join us.</p>
            <button className="text-white">Apply For Internship</button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default About;
