import React,{useState} from "react";
import applyImg from "../assets/apply.jpg";

function Apply() {

  const [formValues, setformValues] = useState({
    fullname: "",
    collegename: "",
    email: "",
    contact: "",
    qualification: "",
    branch:"",
    message: ""
  });

  const [formErrors,setFormErrors] = useState({})

  const handleChange = (e) => {
    const { name, value } = e.target;
    setformValues({ ...formValues, [name]: value });
  }


  const validate = (values) => {
    const errors = {};
    const emailregex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
    if (!values?.fullname) {
      errors.fullname = " Full Name is required";
    }

    if (!values?.collegename) {
      errors.collegename = " College Name is required";
    }

    if (!values?.email) {
      errors.email = "Email is required";
    } else if (!emailregex.test(values.email)) {
      errors.email = "email format 'abcd@domainname.com'";
    }

    if (!values?.contact) {
      errors.contact = "  Mobile Number is required";
    } else if (values?.contact.length < 10) {
      errors.contact = "Number should be 10 digits";
    } else if (values?.contact.length > 10) {
      errors.contact = "Number should be 10 digits";
    }

    if (!values?.qualification) {
      errors.qualification = "Please select qualification from the list";
    }

    if (!values?.branch) {
      errors.branch = "Please select department from the list";
    }

    if (!values?.message) {
      errors.message = "Message required";
    }

    return errors;
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const validated = validate(formValues);
    setFormErrors(validated);

    if(Object.keys(validated).length === 0){
      setformValues({
        fullname: "",
        collegename: "",
        email: "",
        contact: "",
        qualification: "",
        branch:"",
        message:""
      })
      alert('Submitted Successfully');
    }
  }

  return (
    <div className="apply_wrapper">
      <div className="container">
        <div className="apply_content py-5">
          <div className="row">
          <div className="col-1">&nbsp;</div>
            <div className="col-4 px-5">
              <h2>
                Apply For <br /> Internship
              </h2>
              <p>
                Gain enough knowledge to help scale through the tech industry
              </p>
              <img src={applyImg} style={{ width: 300 }} />
            </div>
            
            <div className="col-5">
              <div>
                <form onSubmit={handleSubmit}>
                  <div>
                    <label htmlFor="name">Full Name*</label>
                    <input
                      class="form-control"
                      id="name"
                      type="text"
                      name="fullname"
                      value={formValues?.fullname || ""}
                      onChange={handleChange}
                    />
                    <p className="form_error">{formErrors?.fullname}</p>
                  </div>
                  <div>
                    <label htmlFor="name">College Name*</label>
                    <input
                      class="form-control"
                      id="collegename"
                      type="text"
                      name="collegename"
                      value={formValues?.collegename || ""}
                      onChange={handleChange}
                    />
                    <p className="form_error">{formErrors?.collegename}</p>
                  </div>
                  <div>
                    <label htmlFor="contact">Contact No*</label>
                    <input
                      class="form-control"
                      id="contact"
                      type="number"
                      name="contact"
                      value={formValues?.contact || ""}
                      onChange={handleChange}
                    />
                    <p className="form_error">{formErrors?.contact}</p>
                  </div>
                  <div>
                    <label htmlFor="email">Email Id*</label>
                    <input 
                    type="email" 
                    class="form-control" 
                    id="email" 
                    name="email"
                    value={formValues?.email || ""}
                    onChange={handleChange}
                    />
                    <p className="form_error">{formErrors?.email}</p>
                  </div>
                  <div>
                    <select 
                    class="form-select"    
                    name="qualification"         
                    value={formValues?.qualification || ""}
                    onChange={handleChange} 
                    >
                      <option value="">Qualification</option>
                      <option value="DropOut/Failed">DropOut/Failed</option>
                      <option value="Undergraduate">Undergraduate</option>
                      <option value="Graduate">Graduate</option>
                      <option value="Intermediate">Intermediate</option>
                    </select>
                    <p className="form_error_select">{formErrors?.qualification}</p>
                  </div>
                  <div>
                    <select 
                    class="form-select"
                    name="branch"    
                    value={formValues?.branch || ""}
                    onChange={handleChange}
                    >
                      <option value="1">Engineering Department</option>
                      <option value="ECE">ECE</option>
                      <option value="EEE">EEE</option>
                      <option value="Mechanical">Mechanical</option>
                      <option value="CSE">CSE</option>
                    </select>
                    <p className="form_error_select">{formErrors?.branch}</p>
                  </div>
                  <div>
                    <label htmlFor="message">Expectation from Inernship</label>
                    <textarea
                      class="form-control"
                      id="message"
                      name="message"
                      value={formValues?.message || ""}
                      onChange={handleChange}
                    ></textarea>
                    <p className="form_error_message">{formErrors?.message}</p>
                  </div>
                  <div className="sub_btn">
                    <button className="text-white" type="submit" >Submit</button>
                  </div>
                </form>
              </div>
            </div>
            <div className="col-1">&nbsp;</div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Apply;
